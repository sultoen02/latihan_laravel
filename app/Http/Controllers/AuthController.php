<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function halaman_register() {
        return view('page.form');
    }

    public function halaman_welcome(Request $request) {
        $nama_awal      = $request['first_name'];
        $nama_akhir     = $request['last_name'];
        $jenis_kelamin  = $request['gender'];
        $kebangsaan     = $request['nationality'];
        $bahasa         = $request['language'];
        $biodata        = $request['bio'];
        return view('page.welcome', compact('nama_awal', 'nama_akhir', 'jenis_kelamin', 'kebangsaan', 'bahasa', 'biodata'));
    }
}
