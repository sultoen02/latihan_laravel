@extends('layout.master')

@section('title')
    Show Post {{$cast->id}}
@endsection
@section('content')
    <h4>{{$cast->nama}}</h4>
    <h6>{{$cast->umur}}</h6>
    <p>{{$cast->bio}}</p>
@endsection